# Back-End Test Mentes Notáveis

## Introdução

Esta aplicação em PHP puro foi a parte final que fiz do teste de admição do Mentes Notáveis. Consiste em uma API Restful que manipula um usuário e retorna os endereços, cidades e estados cadastrados no banco de dados.

Agradeço a Mentes Notáveis pela oportunidade.

## Instalando o Projeto após clonar

Diferente do Laravel, neste projeto só precisamos clonar e rodar o script no Workbench para criar o banco com os dados mock ups, este script está na raíz do projeto com nome db_script

## Dados Mock Ups

Os dados são 3 estados e 5 cidades, assim como feito no Laravel.

Os estados são, respectivamente com os ids 1,2,3: 

- São Paulo
- Rio de Janeiro
- Distrito Federal

E as cidades, com mesmo esquema de identificação:

- São Carlos
- São Paulo
- Rio de Janeiro
- Petrópolis
- Brasília

## Rotas

As rotas tem como base o nome da pasta do projeto, portanto ficaria
- GET      | \{nome-da-pasta-raiz}/index.php/addresses
- GET      | \{nome-da-pasta-raiz}/index.php/addresses?id=\{address_id}
- GET      | \{nome-da-pasta-raiz}/index.php/cities
- GET      | \{nome-da-pasta-raiz}/index.php/cities?id=\{city_id}
- GET      | \{nome-da-pasta-raiz}/index.php/cities?id=\{city_id}/count
- GET      | \{nome-da-pasta-raiz}/index.php/states
- GET      | \{nome-da-pasta-raiz}/index.php/states?id=\{state_id}
- GET      | \{nome-da-pasta-raiz}/index.php/states?id=\{state_id}/count
- POST     | \{nome-da-pasta-raiz}/index.php/users
- GET      | \{nome-da-pasta-raiz}/index.php/users
- GET      | \{nome-da-pasta-raiz}/index.php/users?id=\{user_id}         
- PUT      | \{nome-da-pasta-raiz}/index.php/users?id=\{user_id}
- DELETE   | \{nome-da-pasta-raiz}/index.php/users?id=\{user_id}

## Conclusão

Além do mais esse foi o resultado do teste que foi pedido.
