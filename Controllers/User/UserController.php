<?php

function user($method){
  switch ($method) {
    case 'POST':
      store();
      break;
    case 'PUT':
      update();
      break;
    case 'DELETE':
      delete();
      break;
    case 'GET' && count($_GET) != 0:
      show();
      break;
    case 'GET' && count($_GET) == 0:
      index();
      break;
    default:
      echo json_encode(['error' => '403', 'message' => 'Method Not Allowed']);
      break;
  }
}

function store(){
  require_once 'Conn/connection.php';

  $sql = "INSERT INTO addresses (street,number,zip_code,complement,created_at,updated_at,state_id,city_id)
    VALUES ('".$_POST['street']."', '".$_POST['number']."', '".$_POST['zip_code']."', '".$_POST['complement']."','".date('Y-m-d H-m-s')."',
     '".date('Y-m-d H-m-s')."','".$_POST['state_id']."', '".$_POST['city_id']."')";

  $query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

  if($query) {
    $sql = "INSERT INTO users (name,email,cpf,phone,created_at,updated_at,state_id,city_id,address_id,age)
    VALUES ('".$_POST['name']."', '".$_POST['email']."', '".$_POST['cpf']."', '".$_POST['phone']."','".date('Y-m-d H-m-s')."',
    '".date('Y-m-d H-m-s')."','".$_POST['state_id']."', '".$_POST['city_id']."', '".$conn->insert_id."', '".$_POST['age']."')";

    $query = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    if($query) {
      echo json_encode(['error' => '200', 'message' => 'Users stored']);
    }
  }
}

function show(){
  require_once 'Conn/connection.php';
  $sql = "SELECT * FROM users WHERE deleted_at is null AND id = " . $_GET['id'];
  $query = mysqli_query($conn, $sql);
  if(mysqli_num_rows($query) > 0) {
    echo json_encode(mysqli_fetch_all($query, MYSQLI_ASSOC));
  } else {
    echo json_encode(['error' => '404', 'message' => 'Users not found']);
  }
}

function index(){
  require_once 'Conn/connection.php';
  $sql = "SELECT * FROM users WHERE deleted_at is null";
  $query = mysqli_query($conn, $sql);
  if(mysqli_num_rows($query) > 0) {
    echo json_encode(mysqli_fetch_all($query, MYSQLI_ASSOC));
  } else {
    echo json_encode(['error' => '404', 'message' => 'Users not found']);
  }
}

function update(){
  require_once 'Conn/connection.php';
  $sql = "UPDATE users SET
    name = '".$_GET['name']."',
    email = '".$_POST['email']."',
    cpf = '".$_POST['cpf']."',
    phone = '".$_POST['phone']."',
    state_id = '".$_POST['state_id']."',
    updated_at = '".$_POST['updated_at']."',
    city_id = '".$_POST['city_id']."',
    address_id = '".$_POST['address_id']."',
    age = '".$_POST['age']."',
    WHERE id = " . $_GET['id'];
  $query = mysqli_query($conn, $sql) or die(mysqli_error($conn));
  if($query) {
    echo json_encode(['error' => '200', 'message' => 'Users updated']);
  } else {
    echo json_encode(['error' => '404', 'message' => 'Users not found']);
  }
}

function delete(){
  require_once 'Conn/connection.php';
  $sql = "UPDATE users SET deleted_at = '".date('Y-m-d H-m-s')."' WHERE id = " . $_GET['id'];
  $query = mysqli_query($conn, $sql);
  if($query) {
    echo json_encode(['error' => '200', 'message' => 'Users deleted']);
  } else {
    echo json_encode(['error' => '404', 'message' => 'Users not found']);
  }
}
