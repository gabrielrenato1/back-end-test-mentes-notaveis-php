<?php

function city($method){
  switch ($method) {
    case 'GET' && count($_GET) != 0:
      show();
      break;
    case 'GET' && count($_GET) == 0:
      index();
      break;
    default:
      echo json_encode(['error' => '403', 'message' => 'Method Not Allowed']);
      break;
  }
}

function show(){
  require_once 'Conn/connection.php';
  $sql = "SELECT * FROM cities WHERE deleted_at is null AND id = " . $_GET['id'];
  $query = mysqli_query($conn, $sql);
  if(mysqli_num_rows($query) > 0) {
    echo json_encode(mysqli_fetch_all($query, MYSQLI_ASSOC));
  } else {
    echo json_encode(['error' => '404', 'message' => 'City not found']);
  }
}

function index(){
  require_once 'Conn/connection.php';
  $sql = "SELECT * FROM cities WHERE deleted_at is null";
  $query = mysqli_query($conn, $sql);
  if(mysqli_num_rows($query) > 0) {
    echo json_encode(mysqli_fetch_all($query, MYSQLI_ASSOC));
  } else {
    echo json_encode(['error' => '404', 'message' => 'Cities not found']);
  }
}

function usersCount(){
  require_once 'Conn/connection.php';
  $sql = "SELECT * FROM users WHERE deleted_at is null AND city_id = " . $_GET['id'];
  $query = mysqli_query($conn, $sql);

  echo json_encode(["city_id" => $_GET['id'], "count" => mysqli_num_rows($query)]);
}
