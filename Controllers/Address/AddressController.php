<?php

function address($method){
  switch ($method) {
    case 'GET' && count($_GET) != 0:
      show();
      break;
    case 'GET' && count($_GET) == 0:
      index();
      break;
    default:
      echo json_encode(['error' => '405', 'message' => 'Method Not Allowed']);
      break;
  }
}

function show(){
  require_once 'Conn/connection.php';
  $sql = "SELECT * FROM addresses WHERE deleted_at is null AND id = " . $_GET['id'];
  $query = mysqli_query($conn, $sql);
  if(mysqli_num_rows($query) > 0) {
    echo json_encode(mysqli_fetch_all($query, MYSQLI_ASSOC));
  } else {
    echo json_encode(['error' => '404', 'message' => 'Address not found']);
  }
}

function index(){
  require_once 'Conn/connection.php';
  $sql = "SELECT * FROM addresses WHERE deleted_at is null";
  $query = mysqli_query($conn, $sql);
  if(mysqli_num_rows($query) > 0) {
    echo json_encode(mysqli_fetch_all($query, MYSQLI_ASSOC));
  } else {
    echo json_encode(['error' => '404', 'message' => 'Addresses not found']);
  }
}
