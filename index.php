<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function router(){
  switch ($_SERVER['PATH_INFO']) {
    case '/users':
      require 'Controllers/User/UserController.php';
      user($_SERVER['REQUEST_METHOD']);
      break;
    case '/addresses':
      require 'Controllers/Address/AddressController.php';
      address($_SERVER['REQUEST_METHOD']);
      break;
    case '/cities':
      require 'Controllers/City/CityController.php';
      city($_SERVER['REQUEST_METHOD']);
      break;
    case '/states':
      require 'Controllers/State/StateController.php';
      state($_SERVER['REQUEST_METHOD']);
      break;
    case '/states/count':
      require 'Controllers/City/CityController.php';
      usersCount();
      break;
    case '/cities/count':
      require 'Controllers/State/StateController.php';
      usersCount();
      break;
    default:
      echo json_encode(['error' => '404', 'message' => 'Page not found']);
      break;
  }
}

router();

?>
